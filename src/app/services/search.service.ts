import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  API_URL = 'https://api.myjson.com/bins/tl0bp';

  constructor( private http: HttpClient ) { }
  getData() {
    return this.http.get(this.API_URL);
  }
}
