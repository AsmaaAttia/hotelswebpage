import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotelsMenuComponent } from './hotels-menu.component';

describe('HotelsMenuComponent', () => {
  let component: HotelsMenuComponent;
  let fixture: ComponentFixture<HotelsMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotelsMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotelsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
