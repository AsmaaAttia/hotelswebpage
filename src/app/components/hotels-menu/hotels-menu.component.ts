import { Component, OnInit } from "@angular/core";
import { SearchService } from "../../services/search.service";
import { StarRatingComponent } from "ng-starrating";

@Component({
  selector: "app-hotels-menu",
  templateUrl: "./hotels-menu.component.html",
  styleUrls: ["./hotels-menu.component.css"]
})
export class HotelsMenuComponent implements OnInit {
  data: any;
  hotels: any;
  oldValue: any;
  newValue: any;
  checkedColor: any;
  unCheckedColor: any;
  constructor(private searchService: SearchService) {}

  ngOnInit() {
    this.searchService.getData().subscribe(Data => {
      this.data = Data;
      this.hotels = Data["hotels"];
      console.log(this.hotels);
    });
  }
  onRate($event: {
    oldValue: number;
    newValue: number;
    starRating: StarRatingComponent;
  }) {
    this.oldValue = $event.oldValue;
    this.newValue = $event.newValue;
    this.checkedColor = $event.starRating.checkedcolor;
    this.unCheckedColor = $event.starRating.uncheckedcolor;
  }
}
