import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { SearchService } from "../../services/search.service";
import $ from "jquery";

@Component({
  selector: "app-search-hotels",
  templateUrl: "./search-hotels.component.html",
  styleUrls: ["./search-hotels.component.css"]
})
export class SearchHotelsComponent implements OnInit {
  constructor(private searchService: SearchService, private router: Router) {}

  ngOnInit() {}

  AddHotelMenu() {
    if (this.searchService.getData()) {
      this.router.navigate(["/hotels"]);
    } else {
      console.log("error");
    }
  }

  setupDate() {
    let firstDate = $("#firstDate").val();
    console.log(firstDate);
    let secondDate = $("#secondDate").val();
    const findTheDifferenceBetweenTwoDates = (firstDate, secondDate) => {
      firstDate = new Date(firstDate);
      secondDate = new Date(secondDate);

      let timeDifference = Math.abs(secondDate.getTime() - firstDate.getTime());

      let millisecondsInADay = 1000 * 3600 * 24;

      let differenceOfDays = Math.ceil(timeDifference / millisecondsInADay);

      return differenceOfDays;
    };

    let result = findTheDifferenceBetweenTwoDates(firstDate, secondDate);
    console.log(result);
    $("#result").text(result);
  }

  // $(document).ready(function () {
  //   $('#submit').click(function () {
  //     setup();
  //   })
  // });
}
