import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { HttpClientModule } from "@angular/common/http";
import { AppComponent } from "./app.component";
import { SearchHotelsComponent } from "./components/search-hotels/search-hotels.component";
import { HotelsMenuComponent } from "./components/hotels-menu/hotels-menu.component";
import { HomeComponent } from "./components/home/home.component";
import { RatingModule } from "ng-starrating";
import { NgSelectModule } from "ng-custom-select";
// import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';

@NgModule({
  declarations: [
    AppComponent,
    SearchHotelsComponent,
    HotelsMenuComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    // OwlDateTimeModule,
    // OwlNativeDateTimeModule,
    RatingModule,
    NgSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
