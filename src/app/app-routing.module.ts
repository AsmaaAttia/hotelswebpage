import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { SearchHotelsComponent } from "./components/search-hotels/search-hotels.component";
import { HotelsMenuComponent } from "./components/hotels-menu/hotels-menu.component";
import { HomeComponent } from "./components/home/home.component";

const routes: Routes = [
  { path: "searchHotels", component: SearchHotelsComponent },
  { path: "hotels", component: HotelsMenuComponent },
  { path: "", component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
